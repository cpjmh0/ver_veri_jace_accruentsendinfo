package com.accruent.telemetry;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.backup.BBackupService;
import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.job.JobLog;
import javax.baja.sys.BString;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

public class BAccruentBackup
    extends BBackupService
{
  /*-
   class AccruentBackup
   {
     properties
     {
         backupFileName : BString
         default {[BString.make("")]}

     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.BAccruentBackup(3768046223)1.0$ @*/
/* Generated Fri May 03 09:18:03 CDT 2019 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "backupFileName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>backupFileName</code> property.
   * @see com.accruent.telemetry.BAccruentBackup#getBackupFileName
   * @see com.accruent.telemetry.BAccruentBackup#setBackupFileName
   */
  public static final Property backupFileName = newProperty(0, ((BString)(BString.make(""))).getString(),null);
  
  /**
   * Get the <code>backupFileName</code> property.
   * @see com.accruent.telemetry.BAccruentBackup#backupFileName
   */
  public String getBackupFileName() { return getString(backupFileName); }
  
  /**
   * Set the <code>backupFileName</code> property.
   * @see com.accruent.telemetry.BAccruentBackup#backupFileName
   */
  public void setBackupFileName(String v) { setString(backupFileName,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentBackup.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  
  static Logger logger = Logger.getLogger("BAccruentBackup.class");

  BAccruentBackup(String backupFileNamep)
  {
    logger.log(Level.SEVERE, String.format("%s", "Accruent Backup Service Started"));
    setBackupFileName(backupFileNamep); 
  }
  
  @Override
  public void serviceStarted()
  {
    FilePath fp = new FilePath(this.getBackupFileName());
    BIFile biFileOut = null;
    try
    {
      biFileOut = BFileSystem.INSTANCE.makeFile(fp);
      for(BIFile bif : this.listStationBackupFiles())
      {
        if(bif.getFileName().contains("config.bog"))
        {
           logger.log(Level.INFO, String.format("*****Found Config File = %s", bif.getFilePath()));
           DataInputStream dis = new DataInputStream(bif.getInputStream());
           DataOutputStream dos = new DataOutputStream(biFileOut.getOutputStream());
           byte[] bIn = null;
           while(dis.available() != 0)
           {
             bIn = new byte[dis.available()];
             dis.read(bIn);
             dos.write(bIn);
           }
           dis.close();
           dos.flush();
           dos.close();
        }   
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

}
