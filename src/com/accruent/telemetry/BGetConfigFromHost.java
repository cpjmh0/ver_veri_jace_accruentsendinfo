package com.accruent.telemetry;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.file.FilePath;
import javax.baja.sys.Action;
import javax.baja.sys.BComponent;
import javax.baja.sys.BString;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.accruent.telemetry.data.BAccruentFileHandler;
import com.accruent.telemetry.data.BHostConfigFile;
import com.accruent.telemetry.worker.BTelemetryWorker;

public class BGetConfigFromHost
    extends BComponent
{
  /*-
   class BGetConfigFromHost
   {
     properties
     {
         remoteSocketInfo : BRemoteSocketInfo
         default{[new BRemoteSocketInfo()]}
         
         asyncHandler : BTelemetryWorker
         default {[new BTelemetryWorker()]}
     }
     actions
     {
         getConfigFromHost() : BHostConfigFile
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.BGetConfigFromHost(1519871445)1.0$ @*/
/* Generated Fri Jul 14 10:27:29 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "remoteSocketInfo"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>remoteSocketInfo</code> property.
   * @see com.accruent.telemetry.BGetConfigFromHost#getRemoteSocketInfo
   * @see com.accruent.telemetry.BGetConfigFromHost#setRemoteSocketInfo
   */
  public static final Property remoteSocketInfo = newProperty(0, new BRemoteSocketInfo(),null);
  
  /**
   * Get the <code>remoteSocketInfo</code> property.
   * @see com.accruent.telemetry.BGetConfigFromHost#remoteSocketInfo
   */
  public BRemoteSocketInfo getRemoteSocketInfo() { return (BRemoteSocketInfo)get(remoteSocketInfo); }
  
  /**
   * Set the <code>remoteSocketInfo</code> property.
   * @see com.accruent.telemetry.BGetConfigFromHost#remoteSocketInfo
   */
  public void setRemoteSocketInfo(BRemoteSocketInfo v) { set(remoteSocketInfo,v,null); }

////////////////////////////////////////////////////////////////
// Property "asyncHandler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BGetConfigFromHost#getAsyncHandler
   * @see com.accruent.telemetry.BGetConfigFromHost#setAsyncHandler
   */
  public static final Property asyncHandler = newProperty(0, new BTelemetryWorker(),null);
  
  /**
   * Get the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BGetConfigFromHost#asyncHandler
   */
  public BTelemetryWorker getAsyncHandler() { return (BTelemetryWorker)get(asyncHandler); }
  
  /**
   * Set the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BGetConfigFromHost#asyncHandler
   */
  public void setAsyncHandler(BTelemetryWorker v) { set(asyncHandler,v,null); }

////////////////////////////////////////////////////////////////
// Action "getConfigFromHost"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>getConfigFromHost</code> action.
   * @see com.accruent.telemetry.BGetConfigFromHost#getConfigFromHost()
   */
  public static final Action getConfigFromHost = newAction(0,null);
  
  /**
   * Invoke the <code>getConfigFromHost</code> action.
   * @see com.accruent.telemetry.BGetConfigFromHost#getConfigFromHost
   */
  public BHostConfigFile getConfigFromHost() { return (BHostConfigFile)invoke(getConfigFromHost,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BGetConfigFromHost.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  static Logger logger = Logger.getLogger("BGetConfigFromHost.class");
  
  public BGetConfigFromHost()
  {
    
  }
  
  public BHostConfigFile doGetConfigFromHost()
  {
    BAccruentFileHandler bAccruentFileHandler = null;
    String action = null;
    Socket hostSocket = null;
    DataInputStream dis = null;
    DataOutputStream dos = null;
    byte[] bIn = null;
    StringBuffer sbIn = null;
    String[] saIn = null;
    BHostConfigFile bHostConfigFile = null;
    System.out.println(String.format("%s %d",this.getRemoteSocketInfo().getRemoteIpAddress(), this.getRemoteSocketInfo().getRemotePortNumber()));
    try
    {
       hostSocket = new Socket(this.getRemoteSocketInfo().getRemoteIpAddress(), this.getRemoteSocketInfo().getRemotePortNumber());
       hostSocket.setSoTimeout(10000);
       action = String.format("GETCONFIG FOR|%s|%s", Sys.getStation().getStationName(), InetAddress.getLocalHost().getHostAddress());
       dos = new DataOutputStream(hostSocket.getOutputStream());
       dis = new DataInputStream(hostSocket.getInputStream());
       dos.write(action.getBytes());
       dos.flush();
       Thread.sleep(1000);
       while(dis.available() > 0)
       {
         bIn = new byte[dis.available()];
         dis.readFully(bIn, 0, dis.available());
         if(null == sbIn)
         {
           sbIn = new StringBuffer(new String(bIn));
         }
         else
         {  
            sbIn.append(new String(bIn));
         }   
         Thread.sleep(1000);
       }
       dis.close();
       dos.close();
       hostSocket.close();
       if(null != sbIn)
       {
          BufferedOutputStream bos = null;
          bAccruentFileHandler = new BAccruentFileHandler(new FilePath("^AccruentData/configuredOrds.txt"), logger);
          bos = new BufferedOutputStream(bAccruentFileHandler.getDataFile().getOutputStream());
          saIn = sbIn.toString().split("\n");
          for(String s : saIn)
          {
            System.out.println(s);
            bos.write(s.getBytes());
          }
          bos.flush();
          bos.close();
       }
    }
    catch(Exception e)
    {
      logger.log(Level.SEVERE, e.getMessage(), e.getCause());
    }
    return bHostConfigFile;
  }


}
