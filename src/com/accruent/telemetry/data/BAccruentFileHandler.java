package com.accruent.telemetry.data;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.sys.BComponent;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

public class BAccruentFileHandler
    extends BComponent
{
  /*-
   class BAccruentFileHandler
   {
     properties
     {
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.data.BAccruentFileHandler(3289968121)1.0$ @*/
/* Generated Mon Jun 12 16:31:32 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentFileHandler.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
 
//FilePath dataOrds = new FilePath("^AccruentData/configuredOrds.txt");
BIFile dataFile = null;
public BAccruentFileHandler()
{
  
}

public BAccruentFileHandler(FilePath filePath, Logger logger)
{
   try
   {
     //check to see if the file exists.  If not then create it.
     if(null == (dataFile = BFileSystem.INSTANCE.findFile(filePath)))
     {
        dataFile = BFileSystem.INSTANCE.makeFile(filePath);
     }
   }
   catch(Exception e)
   {
      logger.log(Level.SEVERE, e.getMessage()); 
   }
}


public BIFile getDataFile()
{
  return dataFile;
}

public void setDataFile(BIFile dataFile)
{
  this.dataFile = dataFile;
}

}
