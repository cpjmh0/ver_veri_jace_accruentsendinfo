package com.accruent.telemetry.data;

import java.util.ArrayList;

import javax.baja.sys.BComponent;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

public class BHostConfigFile
    extends BComponent
{
  /*-
   class BHostConfigFile
   {
     properties
     {
         ord : String
         default {[new String()]}
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.data.BHostConfigFile(1789577023)1.0$ @*/
/* Generated Tue Jun 06 09:53:37 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "ord"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>ord</code> property.
   * @see com.accruent.telemetry.data.BHostConfigFile#getOrd
   * @see com.accruent.telemetry.data.BHostConfigFile#setOrd
   */
  public static final Property ord = newProperty(0, new String(),null);
  
  /**
   * Get the <code>ord</code> property.
   * @see com.accruent.telemetry.data.BHostConfigFile#ord
   */
  public String getOrd() { return getString(ord); }
  
  /**
   * Set the <code>ord</code> property.
   * @see com.accruent.telemetry.data.BHostConfigFile#ord
   */
  public void setOrd(String v) { setString(ord,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BHostConfigFile.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

   ArrayList<BHostConfigFile> alBHostConfigFile = null;

  public ArrayList<BHostConfigFile> getAlBHostConfigFile()
  {
    return alBHostConfigFile;
  }

  public void setAlBHostConfigFile(ArrayList<BHostConfigFile> alBHostConfigFile)
  {
    if(null == alBHostConfigFile)
    {
      this.alBHostConfigFile = new ArrayList<BHostConfigFile>();
    }
    this.alBHostConfigFile = alBHostConfigFile;
  }
  
}
