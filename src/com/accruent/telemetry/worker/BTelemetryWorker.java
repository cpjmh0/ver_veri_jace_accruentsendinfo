package com.accruent.telemetry.worker;

import javax.baja.util.BWorker;
import javax.baja.util.Worker;
import javax.baja.sys.*;
import javax.baja.util.*;

public class BTelemetryWorker
    extends BWorker
{
  /*-
   class BTelemetryWorker
   {
     properties
     {
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.worker.BTelemetryWorker(2358145291)1.0$ @*/
/* Generated Thu Jun 08 13:07:44 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BTelemetryWorker.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
private CoalesceQueue queue;
private Worker worker;


  @Override
  public Worker getWorker()
  {
    if(null == worker)
    {
      queue = new CoalesceQueue(1000);
      worker = new Worker(queue);
    }
    return worker;
  }

  public void postWork(Runnable r)
  {
    if (null == queue || !isRunning())
    {
      throw new NotRunningException();
    }
    queue.enqueue(r);
  }
}
