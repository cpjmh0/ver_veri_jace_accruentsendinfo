package com.accruent.telemetry.service;

import javax.baja.naming.BOrd;
import javax.baja.sys.BComponent;
import javax.baja.xml.XElem;
import javax.baja.xml.XParser;

public class ParserTester
{

  /**
   * @param args
   */
  public static void main(String[] args)
  {
      String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                 + "<!DOCTYPE site_controller_data_response>"
                 + "<site_controller_data_response>"
                 + "<description>Site Controller Data Import</description>"
                 + "<copyright>Verisae, Inc.</copyright>"
                 + "<date_processed>07/07/2017 3:16:02 PM</date_processed>"
                 + "<request_url>http://192.168.16.209:8040/DataNett/action/siteControllerDataImport</request_url>"
                 + "<headend_server><![CDATA[JACE]]></headend_server>"
                 + "<client>"
                 + "<client_name><![CDATA[Retail Co]]></client_name>"
                 + "</client>"
                 + "<footer site_inserts=\"0\" site_updates=\"0\" channel_inserts=\"0\" channel_updates=\"0\" data_inserts=\"0\" data_updates=\"0\" total_millis=\"157.0\" start_time=\"2017-07-07 15:16:02.939\" end_time=\"2017-07-07 15:16:03.096\" />"
                 + "</site_controller_data_response>";
      try
      {
         XParser xmlParser = XParser.make(xml);
         /*
         System.out.println(XParser.ELEM_START); 1
         System.out.println(XParser.ELEM_END); 2
         System.out.println(XParser.TEXT); 3
         System.out.println(XParser.EOF); -1
         */
         while(xmlParser.next() != XParser.EOF)
         {
           if(xmlParser.type() == 1)
           {
              XElem xElm = xmlParser.elem();
              if(xElm.attrSize() != 0)
              {
                for(int iX=0; iX< xElm.attrSize();iX++)
                {
                  System.out.println(String.format("Attributes = %s %s", xElm.attrName(iX), xElm.attrValue(iX)));
                }
              }
           }
           else if(xmlParser.type() == 3)
           {
             System.out.println(String.format("Names = %s %s",  xmlParser.elem(), xmlParser.text()));
           }
         }
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
  }
}
