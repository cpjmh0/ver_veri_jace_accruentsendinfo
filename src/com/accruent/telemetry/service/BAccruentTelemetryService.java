package com.accruent.telemetry.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.collection.BITable;
import javax.baja.control.BBooleanWritable;
import javax.baja.control.BNumericPoint;
import javax.baja.control.BNumericWritable;
import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.history.BBooleanTrendRecord;
import javax.baja.history.BHistoryConfig;
import javax.baja.history.BHistoryId;
import javax.baja.history.BHistoryRecord;
import javax.baja.history.BHistoryService;
import javax.baja.history.BIHistory;
import javax.baja.history.BNumericTrendRecord;
import javax.baja.history.db.BHistoryDatabase;
import javax.baja.naming.BOrd;
import javax.baja.sys.Action;
import javax.baja.sys.BAbsTime;
import javax.baja.sys.BAbstractService;
import javax.baja.sys.BBoolean;
import javax.baja.sys.BComponent;
import javax.baja.sys.BFacets;
import javax.baja.sys.BRelTime;
import javax.baja.sys.BValue;
import javax.baja.sys.Clock;
import javax.baja.sys.Clock.Ticket;
import javax.baja.sys.Context;
import javax.baja.sys.Cursor;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.units.BUnit;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;
import javax.baja.xml.XElem;
import javax.baja.xml.XParser;
import javax.baja.xml.XText;
import javax.baja.xml.XWriter;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.accruent.telemetry.data.BAccruentFileHandler;
import com.accruent.telemetry.worker.BTelemetryWorker;

public class BAccruentTelemetryService
    extends BAbstractService
{
  /*-
   class BAccruentTelemetryService
   {
     properties
     {
        clientName : String
        default {[new String()]}

        headendServer : String
        default {[new String()]}
        
        webUserName : String
        default {[new String()]}
        
        webUserPassword : String
        default {[new String()]}
        
        webURL : String
        default {[new String()]}
        
        frequencyPerHour : int
        default {[12]}
        
        asyncHandler : BTelemetryWorker
        default {[new BTelemetryWorker()]}

     }
     actions
     {
        sendTelemetryInfo() : BBoolean
        flags {async}
        
        configureModule() : BValue
        flags {summary}

     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.service.BAccruentTelemetryService(323895043)1.0$ @*/
/* Generated Thu Jul 04 12:48:58 CDT 2019 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "clientName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>clientName</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#getClientName
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#setClientName
   */
  public static final Property clientName = newProperty(0, new String(),null);
  
  /**
   * Get the <code>clientName</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#clientName
   */
  public String getClientName() { return getString(clientName); }
  
  /**
   * Set the <code>clientName</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#clientName
   */
  public void setClientName(String v) { setString(clientName,v,null); }

////////////////////////////////////////////////////////////////
// Property "headendServer"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>headendServer</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#getHeadendServer
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#setHeadendServer
   */
  public static final Property headendServer = newProperty(0, new String(),null);
  
  /**
   * Get the <code>headendServer</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#headendServer
   */
  public String getHeadendServer() { return getString(headendServer); }
  
  /**
   * Set the <code>headendServer</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#headendServer
   */
  public void setHeadendServer(String v) { setString(headendServer,v,null); }

////////////////////////////////////////////////////////////////
// Property "webUserName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>webUserName</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#getWebUserName
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#setWebUserName
   */
  public static final Property webUserName = newProperty(0, new String(),null);
  
  /**
   * Get the <code>webUserName</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#webUserName
   */
  public String getWebUserName() { return getString(webUserName); }
  
  /**
   * Set the <code>webUserName</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#webUserName
   */
  public void setWebUserName(String v) { setString(webUserName,v,null); }

////////////////////////////////////////////////////////////////
// Property "webUserPassword"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>webUserPassword</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#getWebUserPassword
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#setWebUserPassword
   */
  public static final Property webUserPassword = newProperty(0, new String(),null);
  
  /**
   * Get the <code>webUserPassword</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#webUserPassword
   */
  public String getWebUserPassword() { return getString(webUserPassword); }
  
  /**
   * Set the <code>webUserPassword</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#webUserPassword
   */
  public void setWebUserPassword(String v) { setString(webUserPassword,v,null); }

////////////////////////////////////////////////////////////////
// Property "webURL"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>webURL</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#getWebURL
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#setWebURL
   */
  public static final Property webURL = newProperty(0, new String(),null);
  
  /**
   * Get the <code>webURL</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#webURL
   */
  public String getWebURL() { return getString(webURL); }
  
  /**
   * Set the <code>webURL</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#webURL
   */
  public void setWebURL(String v) { setString(webURL,v,null); }

////////////////////////////////////////////////////////////////
// Property "frequencyPerHour"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>frequencyPerHour</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#getFrequencyPerHour
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#setFrequencyPerHour
   */
  public static final Property frequencyPerHour = newProperty(0, 12,null);
  
  /**
   * Get the <code>frequencyPerHour</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#frequencyPerHour
   */
  public int getFrequencyPerHour() { return getInt(frequencyPerHour); }
  
  /**
   * Set the <code>frequencyPerHour</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#frequencyPerHour
   */
  public void setFrequencyPerHour(int v) { setInt(frequencyPerHour,v,null); }

////////////////////////////////////////////////////////////////
// Property "asyncHandler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#getAsyncHandler
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#setAsyncHandler
   */
  public static final Property asyncHandler = newProperty(0, new BTelemetryWorker(),null);
  
  /**
   * Get the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#asyncHandler
   */
  public BTelemetryWorker getAsyncHandler() { return (BTelemetryWorker)get(asyncHandler); }
  
  /**
   * Set the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#asyncHandler
   */
  public void setAsyncHandler(BTelemetryWorker v) { set(asyncHandler,v,null); }

////////////////////////////////////////////////////////////////
// Action "sendTelemetryInfo"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>sendTelemetryInfo</code> action.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#sendTelemetryInfo()
   */
  public static final Action sendTelemetryInfo = newAction(Flags.ASYNC,null);
  
  /**
   * Invoke the <code>sendTelemetryInfo</code> action.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#sendTelemetryInfo
   */
  public BBoolean sendTelemetryInfo() { return (BBoolean)invoke(sendTelemetryInfo,null,null); }

////////////////////////////////////////////////////////////////
// Action "configureModule"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>configureModule</code> action.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#configureModule()
   */
  public static final Action configureModule = newAction(Flags.SUMMARY,null);
  
  /**
   * Invoke the <code>configureModule</code> action.
   * @see com.accruent.telemetry.service.BAccruentTelemetryService#configureModule
   */
  public BValue configureModule() { return (BValue)invoke(configureModule,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentTelemetryService.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  static Logger logger = Logger.getLogger("BAccruentTelemetryService.class");
  
  static String SEPARATOR = " : ";
  
  
  static Ticket scheduledActionTicket = null;
  
  public Type[] getServiceTypes()
  {
    return new Type[] {TYPE};
  }
  
  public BValue doConfigureModule()
  {
    BBoolean isConfigured = BBoolean.make("false");
    BufferedReader br = null;
    BIFile fcontrolActionsConfig = BFileSystem.INSTANCE.findFile(new FilePath("^AccruentData/config/telemetryConfig.ini"));
    String inRec = null;
    String key = null;
    String value = null;
    if(null != fcontrolActionsConfig)
    {
      try
      {
         br = new BufferedReader(new InputStreamReader(fcontrolActionsConfig.getInputStream()));
        while (br.ready())
        {
          inRec = br.readLine();
          if (inRec.contains(SEPARATOR))
          {
            key = inRec.split(SEPARATOR)[0];
            value = inRec.split(SEPARATOR)[1];
          }
          if (key.equals("TELEMETRY_CLIENTNAME"))
          {
            setClientName(value);
          }
          else if (key.equals("TELEMETRY_URL"))
          {
            setWebURL(value);
          }
          else if (key.equals("TELEMETRY_FREQUENCY"))
          {
            setFrequencyPerHour(Integer.valueOf(value));
          }
          else if (key.equals("TELEMETRY_HEADENDNAME"))
          {
            setHeadendServer(value);
          }
          else if (key.equals("TELEMETRY_USERNAME"))
          {
            setWebUserName(value);
          }
          else if (key.equals("TELEMETRY_PASSWORD"))
          {
            setWebUserPassword(value);
          }
        }
         isConfigured = BBoolean.make("true");
         setEnabled(Boolean.TRUE);
         this.start();
         Sys.getStation().save();
         br.close();
      }
      catch(Exception e)
      {
        logger.log(Level.SEVERE, String.format("Error Configuring Accruent Alarm Module %s", e.getMessage()));
      }
    }
    else
    {
      setEnabled(Boolean.FALSE);
      setClientName("");
      setHeadendServer("");
      setWebURL("");
      setWebUserName("");
      setWebUserPassword("");
      Sys.getStation().save();
      stop();
    }

    return isConfigured;
  }

  
  public BBoolean doSendTelemetryInfo()
  {
    if(this.isDisabled())
    {
      logger.log(Level.INFO, String.format("This %s service is disabled", this.getName()));
      return BBoolean.TRUE;
    }
    logger.log(Level.INFO, String.format("doSendTelemetryInfo()", ""));
    Context context = this.getSession().getSessionContext();
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    BComponent bComponent = null;
    BNumericPoint bNumericPoint = null;
    BNumericWritable bNumericWritable = null;
    BNumericTrendRecord bNumericTrendRecord = null;
    BBooleanTrendRecord bBooleanTrendRecord = null;
    BBooleanWritable bBooleanWritable = null;
    BFacets bFacets = null;
    BUnit bUnit = null;
    BBoolean isDataSent = BBoolean.make(true);
    BHistoryService bs = (BHistoryService)Sys.getService(BHistoryService.TYPE);
    BHistoryDatabase bdb = bs.getDatabase();
    BHistoryConfig bhc = null;
    ArrayList<String> alConfiguredChannels = null;
    BIHistory biHistory = null;
    HashMap<String, String> hmHistoryIndex = new HashMap<String, String>();
    String ord = null;
    BAbsTime bFirstDate = null;
    BAbsTime bLastDate = null;
    
    String lastUpdatedTime = null;

    SimpleDateFormat dateFormatUTC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    String dateformat = "MM/dd/yyyy HH:mm:ss";
    SimpleDateFormat dateFormatSite = new SimpleDateFormat(dateformat);

    String channelNameSave = null;
    String channelName = null;
    String unitName = null;
    String controllerName = null;
    
    String[] ordInfo = null;
    int ordInfoLength = 0;
    
    XWriter xWriter = null;
    XElem xSiteControllerData = null;
    XElem xClient = null;
    XElem xSiteInformation = null;
    XElem xChannel = null;
    XElem xData = null;
    try
    {
       // see if the lastUpdateTime exists.  If so than we only need to read history records after that time.  Remember, it is returned as UTC time
       // BAbsTime: 2017-07-10 17:37:48.024  Send back the last x number of periods...
       lastUpdatedTime = readLastUpdateTime(logger);
       if(null != lastUpdatedTime)
       {  
         Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
         SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:m:s.S z");
         cal.setTime(sdf.parse(String.format("%s UTC", lastUpdatedTime)));
         bFirstDate = BAbsTime.make(cal.getTimeInMillis()).subtract(BRelTime.makeHours(8));
       }
       alConfiguredChannels = readArrayListConfiguredChannels();
       for(BIHistory bih : bdb.getHistories())
       {  
          if (null != bih) //changed on 4/7.  Pretty weak but seems to work... 
          {
            bhc = bih.getConfig();
            if(!bih.getId().toString().endsWith("_cfg0"))
            {
            hmHistoryIndex.put(bhc.getSource().toString(), bih.getId().toString());
          }
       }
       }

      if (null == alConfiguredChannels) //this will be the case with a brand new unconfigured device.  Need to create a headend.
      {
        // build the xWriter element out of bos...
        xWriter = new XWriter(bos);

        // build the siteControllerDataElement (parent) and add attributes
        xSiteControllerData = new XElem("site_controller_data");
        xSiteControllerData.addAttr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        // took out for testing...
        // xSiteControllerData.addAttr("xsi:noNamespaceSchemaLocation",
        // "https://wbs.verisae.com/DataNett/xsd/siteControllerDataImport.xsd");
        // add headend server to parent
        xSiteControllerData.addContent(buildXElem("headend_server", this.getHeadendServer(), true));

        // build the Client element
        xClient = new XElem("client");

        // add the client name to the client element
        xClient.addContent(buildXElem("client_name", this.getClientName(), true));

        // build the site information element add attributes and sitename and sitedesc
        xSiteInformation = new XElem("site_information");
        xSiteInformation.addAttr("mode", "insert");
        xSiteInformation.addContent(buildXElem("site_name", Sys.getStation().getStationName(), true));
        xSiteInformation.addContent(buildXElem("site_desc", Sys.getStation().getStationName(), true));

      }
      else
      {
        for (String configuredChannel : alConfiguredChannels)
        {
          for (String key : hmHistoryIndex.keySet())
          {
            if (key.contains(configuredChannel))
            {
              

              {
                biHistory = bdb.getHistory(BHistoryId.make(hmHistoryIndex.get(key)));
              if (biHistory.getConfig().getSource().encodeToString().startsWith("station:|"))
              {
                ord = biHistory.getConfig().getSource().toString().split("\\|")[1];
              }
              else
              {
                ord = biHistory.getConfig().getSource().encodeToString();
              }
                System.out.println(String.format("Ord = %s", ord));
                bLastDate = BAbsTime.make();
                BITable collection = (BITable) biHistory.timeQuery(bFirstDate, bLastDate);
                
                if(null != collection)
                {
                    //System.out.println(String.format("Processing Channel %s Key = %s %d", configuredChannel, key, collection.getColumns().size()));
                    Cursor cursor = collection.cursor();
              {
                       while(cursor.next())
              {
                          BHistoryRecord rec = (BHistoryRecord)cursor.get();
                          
                // Make the component and look up the unit and any other pertinent information that
                // we need to send up.
                // We have the BNumericPoint down....
                bComponent = BOrd.make(key.substring(0, key.lastIndexOf('/'))).get().asComponent();
                // we need to get the actual name without any funky characters from the system.
                          String s = rec.getClass().getName();
                if (bComponent.getClass().getName().equals("javax.baja.control.BNumericPoint"))
                {
                  bNumericPoint = (BNumericPoint) bComponent.asComponent();
                  bFacets = bNumericPoint.getFacets();
                  bUnit = (BUnit) bFacets.get("units");
                  unitName = bUnit.getUnitName();
                            channelName = bNumericPoint.getDisplayName(context);
                }
                          else if (bComponent.getClass().getName().equals("javax.baja.control.BNumericWritable"))
                {
                  // pass the class name up so that we can figure out what to do with it next.
                            bNumericWritable = (BNumericWritable)bComponent.asComponent();
                            unitName = bComponent.getClass().getName();
                            bFacets = bNumericWritable.getFacets();
                            bUnit = (BUnit) bFacets.get("units");
                            channelName = bNumericWritable.getDisplayName(context);
                          }
                          else if (bComponent.getClass().getName().equals("javax.baja.control.BBooleanWritable"))
                          {  
                             bBooleanWritable = (BBooleanWritable)bComponent.asComponent();
                  unitName = bComponent.getClass().getName();
                             bFacets = bBooleanWritable.getFacets();
                             bUnit = (BUnit) bFacets.get("units");
                             channelName = bBooleanWritable.getDisplayName(context);
                }
                // check to see if we have started our xml packet
                if (null == xSiteControllerData)
                {
                  // build the xWriter element out of bos...
                  xWriter = new XWriter(bos);

                  // build the siteControllerDataElement (parent) and add attributes
                  xSiteControllerData = new XElem("site_controller_data");
                  xSiteControllerData.addAttr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                  // took out for testing...
                  // xSiteControllerData.addAttr("xsi:noNamespaceSchemaLocation",
                  // "https://wbs.verisae.com/DataNett/xsd/siteControllerDataImport.xsd");
                  // add headend server to parent
                  xSiteControllerData.addContent(buildXElem("headend_server", this.getHeadendServer(), true));

                  // build the Client element
                  xClient = new XElem("client");

                  // add the client name to the client element
                  xClient.addContent(buildXElem("client_name", this.getClientName(), true));

                  // build the site information element add attributes and sitename and sitedesc
                  xSiteInformation = new XElem("site_information");
                  xSiteInformation.addAttr("mode", "insert");
                  xSiteInformation.addContent(buildXElem("site_name", Sys.getStation().getStationName(), true));
                  xSiteInformation.addContent(buildXElem("site_desc", Sys.getStation().getStationName(), true));
                }

                // Generate the channel information now... If null or not equal add a new node.
                ordInfo = ord.split("/");
                          ordInfoLength = ordInfo.length;
                          if (null == channelNameSave || !channelNameSave.equals(ordInfo[ordInfoLength - 2]))
                {
                  if (null != xChannel)
                  {
                    // if(ordInfo[CONTROLLER_NAME].equals("HP_B01"))
                    {
                      xSiteInformation.addContent(xChannel);
                    }
                  }
                            channelNameSave = ordInfo[ordInfoLength - 2];
                  xChannel = new XElem("channel");
                            int iPos = 3;
                            do
                            {
                              controllerName = ordInfo[ordInfoLength - iPos++];
                            }while(controllerName.equals("points"));
                            xChannel.addContent(buildXElem("controller_name", controllerName, true));
                            xChannel.addContent(buildXElem("controller_desc", controllerName, true));
                            xChannel.addContent(buildXElem("channel_name", channelName, true));
                            xChannel.addContent(buildXElem("channel_desc", channelName, true));
                  xChannel.addContent(buildXElem("units", unitName, true));
                            if (rec.getClass().getName().equals("javax.baja.history.BNumericTrendRecord"))
                            {
                              bNumericTrendRecord = (BNumericTrendRecord) rec;
                              // figure out the timestamp and convert to UTC
                              BAbsTime bct = (BAbsTime) bNumericTrendRecord.get("timestamp").asValue();
                              TimeZone siteTimeZone = TimeZone.getTimeZone(bct.getTimeZone().toString().split(" ")[0]);
                              dateFormatSite.setTimeZone(siteTimeZone);
                              Calendar calTemp = new GregorianCalendar();
                              calTemp.setTimeInMillis(bct.getMillis());
                              dateFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
                              Date siteTstamp = dateFormatSite.parse(String.format("%1$tm/%1$td/%1$tY %1$tT", calTemp));
                              String tstampString = dateFormatUTC.format(siteTstamp);
                              XMLGregorianCalendar standardTstamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(tstampString);
                              // build out the xdata element and add it to the xChannel element
                              xData = new XElem("data");
                              xData.addContent(buildXElem("tstamp", standardTstamp.toString(), false));
                              xData.addContent(buildXElem("value", String.valueOf(Math.round(bNumericTrendRecord.getValue())), true));
                              xChannel.addContent(xData);
                }
                            else if(rec.getClass().getName().equals("javax.baja.history.BBooleanTrendRecord"))
                            {
                              bBooleanTrendRecord = (BBooleanTrendRecord) rec;
                              // figure out the timestamp and convert to UTC
                              BAbsTime bct = (BAbsTime) bBooleanTrendRecord.get("timestamp").asValue();
                              TimeZone siteTimeZone = TimeZone.getTimeZone(bct.getTimeZone().toString().split(" ")[0]);
                              dateFormatSite.setTimeZone(siteTimeZone);
                              Calendar calTemp = new GregorianCalendar();
                              calTemp.setTimeInMillis(bct.getMillis());
                              dateFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
                              Date siteTstamp = dateFormatSite.parse(String.format("%1$tm/%1$td/%1$tY %1$tT", calTemp));
                              String tstampString = dateFormatUTC.format(siteTstamp);
                              XMLGregorianCalendar standardTstamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(tstampString);
                              // build out the xdata element and add it to the xChannel element
                              xData = new XElem("data");
                              xData.addContent(buildXElem("tstamp", standardTstamp.toString(), false));
                              xData.addContent(buildXElem("value", String.valueOf(bBooleanTrendRecord.getValue()), true));
                              xChannel.addContent(xData);
                              
                            }
                          }
                else // keep adding to the exisiting channel element
                {
                            if (rec.getClass().getName().equals("javax.baja.history.BNumericTrendRecord"))
                  {
                              bNumericTrendRecord = (BNumericTrendRecord) rec;
                    // figure out the timestamp and convert to UTC
                    BAbsTime bct = (BAbsTime) bNumericTrendRecord.get("timestamp").asValue();
                    TimeZone siteTimeZone = TimeZone.getTimeZone(bct.getTimeZone().toString().split(" ")[0]);
                    dateFormatSite.setTimeZone(siteTimeZone);
                    Calendar calTemp = new GregorianCalendar();
                    calTemp.setTimeInMillis(bct.getMillis());
                    dateFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date siteTstamp = dateFormatSite.parse(String.format("%1$tm/%1$td/%1$tY %1$tT", calTemp));
                    String tstampString = dateFormatUTC.format(siteTstamp);
                    XMLGregorianCalendar standardTstamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(tstampString);
                    // build out the xdata element and add it to the xChannel element
                    xData = new XElem("data");
                    xData.addContent(buildXElem("tstamp", standardTstamp.toString(), false));
                    xData.addContent(buildXElem("value", String.valueOf(Math.round(bNumericTrendRecord.getValue())), true));
                    xChannel.addContent(xData);
                  }
                            else if(rec.getClass().getName().equals("javax.baja.history.BBooleanTrendRecord"))
                            {
                              bBooleanTrendRecord = (BBooleanTrendRecord) rec;
                              // figure out the timestamp and convert to UTC
                              BAbsTime bct = (BAbsTime) bBooleanTrendRecord.get("timestamp").asValue();
                              TimeZone siteTimeZone = TimeZone.getTimeZone(bct.getTimeZone().toString().split(" ")[0]);
                              dateFormatSite.setTimeZone(siteTimeZone);
                              Calendar calTemp = new GregorianCalendar();
                              calTemp.setTimeInMillis(bct.getMillis());
                              dateFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
                              Date siteTstamp = dateFormatSite.parse(String.format("%1$tm/%1$td/%1$tY %1$tT", calTemp));
                              String tstampString = dateFormatUTC.format(siteTstamp);
                              XMLGregorianCalendar standardTstamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(tstampString);
                              // build out the xdata element and add it to the xChannel element
                              xData = new XElem("data");
                              xData.addContent(buildXElem("tstamp", standardTstamp.toString(), false));
                              xData.addContent(buildXElem("value", String.valueOf(bBooleanTrendRecord.getValue()), true));
                              xChannel.addContent(xData);

                }
              }
            }
          }
        }
      }
            }
          }
        }
      }
      
      //add the last xChannel Structure to the xSiteInformation Structure
      if(null != xChannel)
      {  
         xSiteInformation.addContent(xChannel);
      }   

       //if we have info ot add then complete the transaction by sending the info up the pipe
      if (null != xSiteInformation)
      {
        // build out the final xml
        // add the siteInformation to the client
        xClient.addContent(xSiteInformation);
        // add the client to the siteControllerData
        xSiteControllerData.addContent(xClient);
        // write xSiteController to xWriter
        xSiteControllerData.write(xWriter);
        // flush and close xWriter
        xWriter.flush();
        xWriter.close();

        // take out when done debugging....
        BAccruentFileHandler bah =  new BAccruentFileHandler(new FilePath("^AccruentData/sitecontroller.xml"), logger);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(bah.getDataFile().getOutputStream()));
        bw.write(bos.toString());
        bw.flush();
        bw.close();

        // fire the webservice
        isDataSent = isDataSent(this.getWebURL(), this.getWebUserName(), this.getWebUserPassword(), bos.toString());
      }
      else
      {
        
      }
    }   
    catch(Exception e)
    {
      e.printStackTrace();
      logger.log(Level.SEVERE, e.getMessage());
      isDataSent = BBoolean.make(false);
    }
    return isDataSent;
  }
  
  BBoolean isDataSent(String sUrl, String webUserName, String webUserPassword, String xmlData) throws Exception
  {
    BBoolean isDataSent = BBoolean.make(true);
    DataInputStream dis = null;
    DataOutputStream dos = null;
    byte[] bIn = null;
    HttpURLConnection httpUrlConnection = null;
    HttpsURLConnection httpsUrlConnection = null;
    URL url = null;

    //process the xmlDataStream.  Replace all $ with % then url decode it so that when we do the url encoding it is correct
    xmlData = xmlData.replaceAll("\\$", "\\%");
    xmlData = URLDecoder.decode(xmlData, "UTF-8");
    
    //build the url
    sUrl  = String.format("%s?login=%s&password=%s&loginPage=webservice", sUrl, webUserName, webUserPassword);
    url = new URL(sUrl);
    //build the connection
    if(sUrl.startsWith("https"))
    {
       httpsUrlConnection = (HttpsURLConnection)url.openConnection();
       httpsUrlConnection.setRequestMethod("POST");
       httpsUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
       httpsUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
       httpsUrlConnection.setRequestProperty("enctype", "multipart/form-data");
       httpsUrlConnection.setRequestProperty("Content-Length", "0");
       httpsUrlConnection.setDoInput(true);
       httpsUrlConnection.setDoOutput(true);
       httpsUrlConnection.connect();
       dos = new DataOutputStream(httpsUrlConnection.getOutputStream());
       dos.writeBytes(String.format("xml=%s", URLEncoder.encode(xmlData, "UTF-8")));
       dos.flush();
       dos.close();
       dis = new DataInputStream(httpsUrlConnection.getInputStream());
    }
    else
    {
       httpUrlConnection = (HttpURLConnection)url.openConnection();
       httpUrlConnection.setRequestMethod("POST");
       httpUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
       httpUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
       httpUrlConnection.setRequestProperty("enctype", "multipart/form-data");
       httpUrlConnection.setRequestProperty("Content-Length", "0");
       httpUrlConnection.setDoInput(true);
       httpUrlConnection.setDoOutput(true);
       httpUrlConnection.connect();
       dos = new DataOutputStream(httpUrlConnection.getOutputStream());
       dos.writeBytes(String.format("xml=%s", URLEncoder.encode(xmlData, "UTF-8")));
       dos.flush();
       dos.close();
       dis = new DataInputStream(httpUrlConnection.getInputStream());
    }
    Thread.sleep(2000);
    while(dis.available() > 0)
    {
        bIn = new byte[dis.available()];
        dis.read(bIn);
        //logger.log(Level.INFO, new String(bIn));
    }
    XParser xmlResp = XParser.make(new String(bIn));
    while (xmlResp.next() != XParser.EOF)
    {
      if (xmlResp.type() == 1)
      {
        XElem xElm = xmlResp.elem();
        if (xElm.attrSize() != 0)
        {
          for (int iX = 0; iX < xElm.attrSize(); iX++)
          {
            System.out.println(String.format("Attributes = %s %s", xElm.attrName(iX), xElm.attrValue(iX)));
            if(xElm.attrName(iX).equals("end_time"))
            {
              System.out.println(String.format("Attributes = %s %s", xElm.attrName(iX), xElm.attrValue(iX)));
              writeLastUpdateTime(xElm.attrValue(iX), logger);
            }
          }
        }
      }
      else if (xmlResp.type() == 3)
      {
        System.out.println(String.format("Names = %s %s", xmlResp.elem(), xmlResp.text()));
      }
    }
    dis.close();
    return isDataSent;
  }
  /*
   * file is saved in UTC.
   */
  private boolean writeLastUpdateTime(String lastUpdateTime, Logger logger) throws Exception
  {
    boolean isGoodUpdate = true;
    BAccruentFileHandler bAccruentFileHandler = new BAccruentFileHandler(new FilePath("^AccruentData/lastTelemetryUpdateTime.txt"), logger);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(bAccruentFileHandler.getDataFile().getOutputStream()));
    bw.write(lastUpdateTime);
    bw.flush();
    bw.close();
    return isGoodUpdate;
  }
  
  /*
   * file is read in UTC
   */
  private String readLastUpdateTime(Logger logger)
  {
    String lastUpdateTime = null;
    BAccruentFileHandler bAccruentFileHandler = new BAccruentFileHandler(new FilePath("^AccruentData/lastTelemetryUpdateTime.txt"), logger);
    BufferedReader br = null;
    try
    {
      br = new BufferedReader(new InputStreamReader(bAccruentFileHandler.getDataFile().getInputStream()));
      lastUpdateTime = br.readLine();
      br.close();
    }
    catch(Exception e)
    {
       logger.log(Level.INFO, "The lastTelemetryUpdateTime.txt file does not exist");      
    }
    return lastUpdateTime;
  }
  
  private ArrayList<String> readArrayListConfiguredChannels() throws Exception
  {
    ArrayList<String> alConfiguredChannels = null;
    BAccruentFileHandler bAccruentFileHandler = new BAccruentFileHandler(new FilePath("^AccruentData/configuredOrds.txt"), logger);
    BufferedReader br = new BufferedReader(new InputStreamReader(bAccruentFileHandler.getDataFile().getInputStream()));
    String sRec = null;
    String[] saRec = null;
    while(br.ready())
    {
       if(null == alConfiguredChannels)
       {
         alConfiguredChannels = new ArrayList<String>();
       }
       sRec = br.readLine();
       if(sRec.contains("|"))
       {
         saRec = sRec.split("\\|");
         sRec = saRec[0];
         sRec = sRec.replaceAll(" ", "\\$20");
       }
       alConfiguredChannels.add(sRec);
    }
    return alConfiguredChannels;
  }
  
  @Override
  public IFuture post(Action action, BValue value, Context cx)
  {
    System.out.println("IN THE THREAD");
    Invocation work = new Invocation(this, action, value, cx);
    getAsyncHandler().postWork(work);
    return null;
  }
  
  @Override
  public void serviceStarted()
  {
    BBoolean isConfigured = (BBoolean)doConfigureModule(); 
    if(isConfigured.getBoolean())
    {  
      logger.log(Level.INFO, String.format("*************The AccruentSendTelemetry Service has Started", ""));
      scheduledActionTicket = Clock.schedulePeriodically(this, BRelTime.makeMinutes(60 / this.getFrequencyPerHour()), this.getAction("sendTelemetryInfo"), null);
    }   
    else
    {
      setEnabled(Boolean.FALSE);
      setClientName("");
      setHeadendServer("");
      setWebURL("");
      setWebUserName("");
      setWebUserPassword("");
      Sys.getStation().save();
      stop();
    }
  }
  
  @Override
  public void changed(Property property, Context ctx)
  {
    if (this.isRunning() && !this.isDisabled())
    {
      logger.log(Level.INFO, String.format("The propery %s has changed....", property.getName()));
      scheduledActionTicket.cancel();
      scheduledActionTicket = Clock.schedulePeriodically(this, BRelTime.makeMinutes(60 / this.getFrequencyPerHour()), this.getAction("sendTelemetryInfo"), null);
    }
  }
  
  XElem buildXElem(String elementName, String elementData, boolean isCdata) throws Exception
  {
     XElem xElem = new XElem(elementName);
     XText xText = new XText(elementData);
     xText.setCDATA(isCdata);
     xElem.addContent(xText);
     return xElem;
  }


}
