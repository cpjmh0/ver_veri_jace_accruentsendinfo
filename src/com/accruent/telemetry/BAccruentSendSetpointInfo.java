package com.accruent.telemetry;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.control.BBooleanWritable;
import javax.baja.control.BNumericWritable;
import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.naming.BOrd;
import javax.baja.sys.Action;
import javax.baja.sys.BAbstractService;
import javax.baja.sys.BBoolean;
import javax.baja.sys.BComponent;
import javax.baja.sys.BRelTime;
import javax.baja.sys.BString;
import javax.baja.sys.BValue;
import javax.baja.sys.Clock;
import javax.baja.sys.Clock.Ticket;
import javax.baja.sys.Context;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;

import com.accruent.communications.AccruentCommunications;
import com.accruent.communications.AccruentCommunications.CommunicationResults;
import com.accruent.telemetry.data.BAccruentFileHandler;
import com.accruent.telemetry.worker.BTelemetryWorker;
import com.google.gson.Gson;

/*
 * This program will send telemetry information to the accruent system based on the schedule supplied.
 */
public class BAccruentSendSetpointInfo extends BAbstractService
{
  /*-
   class BAccruentSendSetpointInfo
   {
     properties
     {
         clientName : BString
         default {[BString.make("")]}
         
         webURL : BString
         default {[BString.make("")]}
         
         asyncHandler : BTelemetryWorker
         default {[new BTelemetryWorker()]}
         
         numberOfSendsPerDay : int
         default {[1]}
         
     }
     actions
    {
        sendSetpointInfoToHost() : BBoolean
        flags {async}
        
        configureModule() : BValue
        flags {summary}
     }
      
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.BAccruentSendSetpointInfo(1511135531)1.0$ @*/
/* Generated Thu Jul 04 13:12:43 CDT 2019 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "clientName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>clientName</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#getClientName
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#setClientName
   */
  public static final Property clientName = newProperty(0, ((BString)(BString.make(""))).getString(),null);
  
  /**
   * Get the <code>clientName</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#clientName
   */
  public String getClientName() { return getString(clientName); }
  
  /**
   * Set the <code>clientName</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#clientName
   */
  public void setClientName(String v) { setString(clientName,v,null); }

////////////////////////////////////////////////////////////////
// Property "webURL"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>webURL</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#getWebURL
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#setWebURL
   */
  public static final Property webURL = newProperty(0, ((BString)(BString.make(""))).getString(),null);
  
  /**
   * Get the <code>webURL</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#webURL
   */
  public String getWebURL() { return getString(webURL); }
  
  /**
   * Set the <code>webURL</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#webURL
   */
  public void setWebURL(String v) { setString(webURL,v,null); }

////////////////////////////////////////////////////////////////
// Property "asyncHandler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#getAsyncHandler
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#setAsyncHandler
   */
  public static final Property asyncHandler = newProperty(0, new BTelemetryWorker(),null);
  
  /**
   * Get the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#asyncHandler
   */
  public BTelemetryWorker getAsyncHandler() { return (BTelemetryWorker)get(asyncHandler); }
  
  /**
   * Set the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#asyncHandler
   */
  public void setAsyncHandler(BTelemetryWorker v) { set(asyncHandler,v,null); }

////////////////////////////////////////////////////////////////
// Property "numberOfSendsPerDay"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>numberOfSendsPerDay</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#getNumberOfSendsPerDay
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#setNumberOfSendsPerDay
   */
  public static final Property numberOfSendsPerDay = newProperty(0, 1,null);
  
  /**
   * Get the <code>numberOfSendsPerDay</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#numberOfSendsPerDay
   */
  public int getNumberOfSendsPerDay() { return getInt(numberOfSendsPerDay); }
  
  /**
   * Set the <code>numberOfSendsPerDay</code> property.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#numberOfSendsPerDay
   */
  public void setNumberOfSendsPerDay(int v) { setInt(numberOfSendsPerDay,v,null); }

////////////////////////////////////////////////////////////////
// Action "sendSetpointInfoToHost"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>sendSetpointInfoToHost</code> action.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#sendSetpointInfoToHost()
   */
  public static final Action sendSetpointInfoToHost = newAction(Flags.ASYNC,null);
  
  /**
   * Invoke the <code>sendSetpointInfoToHost</code> action.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#sendSetpointInfoToHost
   */
  public BBoolean sendSetpointInfoToHost() { return (BBoolean)invoke(sendSetpointInfoToHost,null,null); }

////////////////////////////////////////////////////////////////
// Action "configureModule"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>configureModule</code> action.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#configureModule()
   */
  public static final Action configureModule = newAction(Flags.SUMMARY,null);
  
  /**
   * Invoke the <code>configureModule</code> action.
   * @see com.accruent.telemetry.BAccruentSendSetpointInfo#configureModule
   */
  public BValue configureModule() { return (BValue)invoke(configureModule,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentSendSetpointInfo.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

static Logger logger = Logger.getLogger("BAccruentSendTelementry.class");  
static Ticket scheduledActionTicket = null;
static String SEPARATOR = " : ";

public BAccruentSendSetpointInfo()
{
  
}

/*
 * (non-Javadoc)
 * @see javax.baja.web.BWebServlet#getServiceTypes()
 * Always declare this when there is a service
 */
public Type[] getServiceTypes()
{
  return new Type[] {TYPE};
}

public IFuture post(Action action, BValue argument, Context cx)
{
  Invocation work = new Invocation(this, action, argument, cx);
  getAsyncHandler().postWork(work);
  return null;
}

public BValue doConfigureModule()
{
  BBoolean isConfigured = BBoolean.make("false");
  BufferedReader br = null;
  BIFile fcontrolActionsConfig = BFileSystem.INSTANCE.findFile(new FilePath("^AccruentData/config/controlActionConfig.ini"));
  String inRec = null;
  String key = null;
  String value = null;
  logger.log(Level.INFO, String.format("*****Configuring the SendSetPointInfo Module******", ""));
  if(null != fcontrolActionsConfig)
  {
    try
    {
       br = new BufferedReader(new InputStreamReader(fcontrolActionsConfig.getInputStream()));
       while(br.ready())
       {
         inRec = br.readLine();
         if(inRec.contains(SEPARATOR))
         {
           key = inRec.split(SEPARATOR)[0];
           value = inRec.split(SEPARATOR)[1];
           logger.log(Level.INFO,String.format("Key = %s  Value = %s", key, value));
         }
         if(key.equals("CONFIG_CLIENTNAME"))
         {
             setClientName(value);
         }
         else if(key.equals("SETPOINT_URL"))
         {  
             setWebURL(value);
         }    
         else if(key.equals("CONFIG_FREQUENCY"))
         {  
             setNumberOfSendsPerDay(Integer.valueOf(value));
         }
       }
       isConfigured = BBoolean.make("true");
       setEnabled(Boolean.TRUE);
       this.start();
       Sys.getStation().save();
       br.close();
    }
    catch(Exception e)
    {
      logger.log(Level.SEVERE, String.format("Error Configuring Accruent Alarm Module %s", e.getMessage()));
    }
  }
  else
  {
    logger.log(Level.INFO, String.format("Could not get the file to configure the service" ));
    logger.log(Level.INFO, String.format("*************The AccruentSendSetPointInfo Service is not configured", ""));
    setEnabled(Boolean.FALSE);
    setClientName("");
    setWebURL("");
    Sys.getStation().save();
    stop();
  }
  return isConfigured;
}


  public BBoolean doSendSetpointInfoToHost()
{
    if (this.isDisabled())
    {
      logger.log(Level.INFO, String.format("This %s service is disabled", this.getName()));
      return BBoolean.TRUE;
        }
    Gson gson = new Gson();
    ArrayList<NiagaraPointValue> alNiagaraPointValues = new ArrayList<NiagaraPointValue>();
  AccruentCommunications accruentCommunications = new AccruentCommunications(this.getWebURL(), logger);
  CommunicationResults communicationResults = null;
  BBoolean isFileSent = BBoolean.make(true);
    ArrayList<BOrd> alBord = null;
    BComponent bPoint = null;
    BOrd bOrd = null;
    String sOrd = null;
    try
    {
      alBord = buildSetpointOrdList();
      for(BOrd b : alBord)
      {  
        //build out the ord to a string
        sOrd = b.toString();
        sOrd = sOrd.replaceAll(" ", "\\$20");
        //build out the ord to a component
        bOrd = BOrd.make(String.format("local:|station:|%s", sOrd));
        bPoint = bOrd.get().asComponent();
        if(bPoint.getClass().getSimpleName().equals("BNumericWritable"))
        {  
          BNumericWritable bnw = (BNumericWritable) bPoint.asComponent(); // generate the component
          alNiagaraPointValues.add(new NiagaraPointValue(sOrd, String.valueOf(bnw.getOut().getValue()), "BNumericWritable", bnw.getFacets().get("units").toString()));
        }
        else if(bPoint.getClass().getSimpleName().equals("BBooleanWritable"))
        {  
            BBooleanWritable bbw = (BBooleanWritable) bPoint.asComponent(); // generate the component
            alNiagaraPointValues.add(new NiagaraPointValue(sOrd, String.valueOf(bbw.getOut().getValue()), "BBooleanWritable", bbw.getFacets().get("units").toString()));
        }  
      }  
      if(accruentCommunications.getBsUrl().contains("https"))
      {
        communicationResults = accruentCommunications.callHostHttps(AccruentCommunications.SEND_CHANNEL_META_DATA, gson.toJson(alNiagaraPointValues), null);
      }
      else
      {  
        communicationResults = accruentCommunications.callHostHttp(AccruentCommunications.SEND_CHANNEL_META_DATA, gson.toJson(alNiagaraPointValues), null);
      }   
      isFileSent = BBoolean.make(communicationResults.isGoodTransmission());
    }
    catch (Exception e)
    {
      isFileSent = BBoolean.make(false);
      logger.log(Level.SEVERE, e.getMessage());
    }
    return isFileSent;
  }

  

  private ArrayList<BOrd> buildSetpointOrdList() throws Exception
{
    ArrayList<BOrd> alBord = new ArrayList<BOrd>();
  BAccruentFileHandler bAccruentFileHandler = new BAccruentFileHandler(new FilePath("^AccruentData/configuredSetpoints.txt"), logger);
  BufferedReader br = new BufferedReader(new InputStreamReader(bAccruentFileHandler.getDataFile().getInputStream()));
  String configuredChannelRecord = null;
  while(br.ready())
  {
     configuredChannelRecord = br.readLine();
     if(configuredChannelRecord.contains("|"))
     {
       configuredChannelRecord = configuredChannelRecord.split("\\|")[0];
        configuredChannelRecord = configuredChannelRecord.replaceAll("\\$", "\\%");
        configuredChannelRecord = URLDecoder.decode(configuredChannelRecord, "UTF-8");
        if(null == alBord)
     {
          alBord = new ArrayList<BOrd>();
     }
        alBord.add(BOrd.make(configuredChannelRecord));
     }
   
  }
    return alBord;
}
  
  @Override
  public void serviceStarted()
  {
    BBoolean isConfigured = (BBoolean)doConfigureModule(); 
    if(isConfigured.getBoolean())
    {  
      logger.log(Level.INFO, String.format("*************The AccruentSendSetPointInfo Service has Started", ""));
      if (this.getNumberOfSendsPerDay() > 24 || this.getNumberOfSendsPerDay() < 1)
      {
        this.setNumberOfSendsPerDay(1);
      }
      scheduledActionTicket = Clock.schedulePeriodically(this, BRelTime.makeHours(this.getNumberOfSendsPerDay()), this.getAction("sendSetpointInfoToHost"), null);
    }   
    else
    {
      logger.log(Level.INFO, String.format("*************The AccruentSendSetPointInfo Service is not configured", ""));
      setEnabled(Boolean.FALSE);
      setClientName("");
      setWebURL("");
      Sys.getStation().save();
      stop();
    }

  }
  
  @Override
  public void changed(Property property, Context ctx)
  {
    if (this.isRunning() && !this.isDisabled())
    {
      logger.log(Level.INFO, String.format("The propery %s has changed....", property.getName()));
      if(this.getNumberOfSendsPerDay() > 24 || this.getNumberOfSendsPerDay() < 1)
      {
        this.setNumberOfSendsPerDay(1);
      }
      scheduledActionTicket.cancel();
      scheduledActionTicket = Clock.schedulePeriodically(this, BRelTime.makeDays(this.getNumberOfSendsPerDay()), this.getAction("sendSetpointInfoToHost"), null);
    }
}

class NiagaraPointValue
{
  String slotPathOrd = null;
  String value = null;
  String type = null;
  String uom = null;
  NiagaraPointValue(String slotPathOrdp, String valuep, String typep, String uomp)
  {
    this.slotPathOrd = slotPathOrdp;
    this.value = valuep;
    this.type = typep;
    this.uom = uomp;
  }
  
  NiagaraPointValue(String slotPathOrdp, String valuep)
  {
    this.slotPathOrd = slotPathOrdp;
    this.value = valuep;
  }
}
}
