package com.accruent.telemetry;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.baja.collection.BITable;
import javax.baja.collection.Column;
import javax.baja.collection.TableCursor;
import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.naming.BOrd;
import javax.baja.sys.Action;
import javax.baja.sys.BAbstractService;
import javax.baja.sys.BBoolean;
import javax.baja.sys.BObject;
import javax.baja.sys.BRelTime;
import javax.baja.sys.BString;
import javax.baja.sys.BValue;
import javax.baja.sys.Clock;
import javax.baja.sys.Clock.Ticket;
import javax.baja.sys.Context;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;

import com.accruent.communications.AccruentCommunications;
import com.accruent.communications.AccruentCommunications.CommunicationResults;
import com.accruent.telemetry.data.BAccruentFileHandler;
import com.accruent.telemetry.worker.BTelemetryWorker;
import com.google.gson.Gson;

public class BAccruentSendConfigToHost
    extends BAbstractService
{
  //took out when moved to http communications....
  //         remoteSocketInfo : BRemoteSocketInfo
  //         default{[new BRemoteSocketInfo()]}

  /*-
   class BAccruentSendConfigToHost
   {
     properties
     {
     
         clientName : BString
         default {[BString.make("")]}
         
         webURL : BString
         default {[BString.make("")]}
d
         asyncHandler : BTelemetryWorker
         default {[new BTelemetryWorker()]}
         
         numberOfSendsPerDay : int
         default {[1]}
         
         backupFileName : BString
         default {[BString.make("")]}

     }
     actions
     {
        sendConfigToHost() : BBoolean
        flags {async}
        
        configureModule() : BValue
        flags {summary}
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.BAccruentSendConfigToHost(1942024536)1.0$ @*/
/* Generated Thu Jul 04 12:58:50 CDT 2019 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "clientName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>clientName</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#getClientName
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#setClientName
   */
  public static final Property clientName = newProperty(0, ((BString)(BString.make(""))).getString(),null);
  
  /**
   * Get the <code>clientName</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#clientName
   */
  public String getClientName() { return getString(clientName); }
  
  /**
   * Set the <code>clientName</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#clientName
   */
  public void setClientName(String v) { setString(clientName,v,null); }

////////////////////////////////////////////////////////////////
// Property "webURL"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>webURL</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#getWebURL
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#setWebURL
   */
  public static final Property webURL = newProperty(0, ((BString)(BString.make(""))).getString(),null);
  
  /**
   * Get the <code>webURL</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#webURL
   */
  public String getWebURL() { return getString(webURL); }
  
  /**
   * Set the <code>webURL</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#webURL
   */
  public void setWebURL(String v) { setString(webURL,v,null); }

////////////////////////////////////////////////////////////////
// Property "asyncHandler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#getAsyncHandler
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#setAsyncHandler
   */
  public static final Property asyncHandler = newProperty(0, new BTelemetryWorker(),null);
  
  /**
   * Get the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#asyncHandler
   */
  public BTelemetryWorker getAsyncHandler() { return (BTelemetryWorker)get(asyncHandler); }
  
  /**
   * Set the <code>asyncHandler</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#asyncHandler
   */
  public void setAsyncHandler(BTelemetryWorker v) { set(asyncHandler,v,null); }

////////////////////////////////////////////////////////////////
// Property "numberOfSendsPerDay"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>numberOfSendsPerDay</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#getNumberOfSendsPerDay
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#setNumberOfSendsPerDay
   */
  public static final Property numberOfSendsPerDay = newProperty(0, 1,null);
  
  /**
   * Get the <code>numberOfSendsPerDay</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#numberOfSendsPerDay
   */
  public int getNumberOfSendsPerDay() { return getInt(numberOfSendsPerDay); }
  
  /**
   * Set the <code>numberOfSendsPerDay</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#numberOfSendsPerDay
   */
  public void setNumberOfSendsPerDay(int v) { setInt(numberOfSendsPerDay,v,null); }

////////////////////////////////////////////////////////////////
// Property "backupFileName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>backupFileName</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#getBackupFileName
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#setBackupFileName
   */
  public static final Property backupFileName = newProperty(0, ((BString)(BString.make(""))).getString(),null);
  
  /**
   * Get the <code>backupFileName</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#backupFileName
   */
  public String getBackupFileName() { return getString(backupFileName); }
  
  /**
   * Set the <code>backupFileName</code> property.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#backupFileName
   */
  public void setBackupFileName(String v) { setString(backupFileName,v,null); }

////////////////////////////////////////////////////////////////
// Action "sendConfigToHost"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>sendConfigToHost</code> action.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#sendConfigToHost()
   */
  public static final Action sendConfigToHost = newAction(Flags.ASYNC,null);
  
  /**
   * Invoke the <code>sendConfigToHost</code> action.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#sendConfigToHost
   */
  public BBoolean sendConfigToHost() { return (BBoolean)invoke(sendConfigToHost,null,null); }

////////////////////////////////////////////////////////////////
// Action "configureModule"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>configureModule</code> action.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#configureModule()
   */
  public static final Action configureModule = newAction(Flags.SUMMARY,null);
  
  /**
   * Invoke the <code>configureModule</code> action.
   * @see com.accruent.telemetry.BAccruentSendConfigToHost#configureModule
   */
  public BValue configureModule() { return (BValue)invoke(configureModule,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentSendConfigToHost.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  
  static Logger logger = Logger.getLogger("BSendConfigToHost.class");
  static Ticket scheduledActionTicket = null;
  static String SEPARATOR = " : ";
  
  public BAccruentSendConfigToHost()
  {
  }
  public BValue doConfigureModule()
  {
    BBoolean isConfigured = BBoolean.make("false");
    BufferedReader br = null;
    BIFile fcontrolActionsConfig = BFileSystem.INSTANCE.findFile(new FilePath("^AccruentData/config/controlActionConfig.ini"));
    String inRec = null;
    String key = null;
    String value = null;
    if(null != fcontrolActionsConfig)
    {
      try
      {
         br = new BufferedReader(new InputStreamReader(fcontrolActionsConfig.getInputStream()));
         while(br.ready())
         {
           inRec = br.readLine();
           if(inRec.contains(SEPARATOR))
           {
             key = inRec.split(SEPARATOR)[0];
             value = inRec.split(SEPARATOR)[1];
           }
           if(key.equals("CONFIG_CLIENTNAME"))
           {  
               setClientName(value);
           }
           else if(key.equals("CONFIG_URL"))
           {
               setWebURL(value);
           }
           else if(key.equals("CONFIG_FREQUENCY"))
           {
               setNumberOfSendsPerDay(Integer.valueOf(value));
           }
         }
         isConfigured = BBoolean.make("true");
         setEnabled(Boolean.TRUE);
         this.start();
         Sys.getStation().save();
         br.close();
      }
      catch(Exception e)
      {
        logger.log(Level.SEVERE, String.format("Error Configuring Accruent Alarm Module %s", e.getMessage()));
      }
    }
    else
    {
      setEnabled(Boolean.FALSE);
      setClientName("");
      setWebURL("");
      setNumberOfSendsPerDay(0);
      Sys.getStation().save();
      stop();
    }

    return isConfigured;
  }

  
  public BBoolean doSendConfigToHost()
  {
    if(this.isDisabled())
    {
      return BBoolean.FALSE;
    }
    //changed this to use http rather than a straight socket connection.  Not yet implemented on the DataNettSide.  Need to build a handler...
    //AccruentCommunications accruentCommunications = new AccruentCommunications(getRemoteSocketInfo(), logger);
    Gson gson = null;
    ByteArrayOutputStream configBos = null;
    AccruentCommunications accruentCommunications = new AccruentCommunications(this.getWebURL(), logger);
    CommunicationResults communicationResults = null;
    BBoolean isFileSent = BBoolean.make(false);
    //Read all of the points from the station
    try
    {
       doBackupConfig();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    
    //listen for response from controller and get a list of the configured devices.
    try
    {
        gson = new Gson();
      if(accruentCommunications.getBsUrl().contains("https"))
      {
            communicationResults = accruentCommunications.callHostHttps(AccruentCommunications.SEND_CHANNEL_META_DATA, "", this.getBackupFileName());
      }
      else
      {  
           communicationResults = accruentCommunications.callHostHttp(AccruentCommunications.SEND_CHANNEL_META_DATA, "", this.getBackupFileName());
      }

       isFileSent = BBoolean.make(communicationResults.isGoodTransmission());
       if(isFileSent.equals(BBoolean.TRUE))
       {  
           String[] sRec = null;
           String[] row = null;
           BAccruentFileHandler bAccruentFileHandler = new BAccruentFileHandler(new FilePath("^AccruentData/configuredOrds.txt"), logger);
           BufferedOutputStream bos = new BufferedOutputStream(bAccruentFileHandler.getDataFile().getOutputStream());;
           BAccruentFileHandler bAccruentSetpointFileHandler = new BAccruentFileHandler(new FilePath("^AccruentData/configuredSetpoints.txt"), logger);
           BufferedOutputStream bosSetpoint = new BufferedOutputStream(bAccruentSetpointFileHandler.getDataFile().getOutputStream());;
           row = communicationResults.getChannelsConfigured().split(String.valueOf(AccruentCommunications.EOT));
           row = gson.fromJson(row[0], String[].class);
           for(String s : row)
           {
              sRec = s.split("\\|");
              if(sRec[2].equals("true"))
              {
                s = s.replaceAll(" ", "\\$20");
              bos.write(String.format("%s\n", s).getBytes());
              }
              else
              {
                s = s.replaceAll(" ", "\\$20");
              bosSetpoint.write(String.format("%s\n", s).getBytes());
           }
           }
           bos.flush();
           bos.close();
           bosSetpoint.flush();
           bosSetpoint.close();
       }
    }
    catch(Exception e)
    {
      e.printStackTrace();
      logger.log(Level.SEVERE, e.getMessage());
    }
    return isFileSent;
  }
  
  public IFuture post(Action action, BValue argument, Context cx)
  {
    Invocation work = new Invocation(this, action, argument, cx);
    getAsyncHandler().postWork(work);
    return null;
  }

  public Type[] getServiceTypes()
  {
    return new Type[] {TYPE};
  }

  public BBoolean doBackupConfig()
  {
     System.out.println(String.format("ClientName = %s  FileName = %s", this.getClientName(), this.getBackupFileName()));
     BAccruentBackup accruentBackup = new BAccruentBackup(this.getBackupFileName());
     accruentBackup.serviceStarted();;
     return BBoolean.TRUE;
  }
  
  @Override
  public void started()
  {
    BBoolean isConfigured = (BBoolean)doConfigureModule(); 
    if(isConfigured.getBoolean())
    {  
      logger.log(Level.INFO, String.format("*************The AccruentSendConfigToHost Service has Started", ""));
      if (this.getNumberOfSendsPerDay() > 24 || this.getNumberOfSendsPerDay() < 1)
      {
        this.setNumberOfSendsPerDay(1);
      }
      scheduledActionTicket = Clock.schedulePeriodically(this, BRelTime.makeHours(this.getNumberOfSendsPerDay()), this.getAction("sendSetpointInfoToHost"), null);
    }   
    else
    {
      logger.log(Level.INFO, String.format("*************The AccruentSendConfigToHost Service is not configured", ""));
      setEnabled(Boolean.FALSE);
      setClientName("");
      setWebURL("");
      this.start();
      Sys.getStation().save();
      stop();
    }

  }
  
  @Override
  public void changed(Property property, Context ctx)
  {
    if (this.isRunning() && !this.isDisabled())
    {
      logger.log(Level.INFO, String.format("The propery %s has changed....", property.getName()));
      if(this.getNumberOfSendsPerDay() > 24 || this.getNumberOfSendsPerDay() < 1)
      {
        this.setNumberOfSendsPerDay(1);
      }
      scheduledActionTicket.cancel();
      scheduledActionTicket = Clock.schedulePeriodically(this, BRelTime.makeHours(this.getNumberOfSendsPerDay()) , this.getAction("sendConfigToHost"), null);
    }
  }
}
