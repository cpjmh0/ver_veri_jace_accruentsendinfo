package com.accruent.telemetry;

import javax.baja.sys.BComponent;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

public class BRemoteSocketInfo
    extends BComponent
{
  /*-
   class BRemoteSocketInfo
   {
     properties
     {
        remoteIpAddress : String
        default{[new String()]}
        
        remotePortNumber : int
        default {[0]}
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.BRemoteSocketInfo(2999768943)1.0$ @*/
/* Generated Wed Jul 05 13:51:32 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "remoteIpAddress"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>remoteIpAddress</code> property.
   * @see com.accruent.telemetry.BRemoteSocketInfo#getRemoteIpAddress
   * @see com.accruent.telemetry.BRemoteSocketInfo#setRemoteIpAddress
   */
  public static final Property remoteIpAddress = newProperty(0, new String(),null);
  
  /**
   * Get the <code>remoteIpAddress</code> property.
   * @see com.accruent.telemetry.BRemoteSocketInfo#remoteIpAddress
   */
  public String getRemoteIpAddress() { return getString(remoteIpAddress); }
  
  /**
   * Set the <code>remoteIpAddress</code> property.
   * @see com.accruent.telemetry.BRemoteSocketInfo#remoteIpAddress
   */
  public void setRemoteIpAddress(String v) { setString(remoteIpAddress,v,null); }

////////////////////////////////////////////////////////////////
// Property "remotePortNumber"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>remotePortNumber</code> property.
   * @see com.accruent.telemetry.BRemoteSocketInfo#getRemotePortNumber
   * @see com.accruent.telemetry.BRemoteSocketInfo#setRemotePortNumber
   */
  public static final Property remotePortNumber = newProperty(0, 0,null);
  
  /**
   * Get the <code>remotePortNumber</code> property.
   * @see com.accruent.telemetry.BRemoteSocketInfo#remotePortNumber
   */
  public int getRemotePortNumber() { return getInt(remotePortNumber); }
  
  /**
   * Set the <code>remotePortNumber</code> property.
   * @see com.accruent.telemetry.BRemoteSocketInfo#remotePortNumber
   */
  public void setRemotePortNumber(int v) { setInt(remotePortNumber,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BRemoteSocketInfo.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  



}