package com.accruent.communications;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.platform.LocalPlatform;
import javax.baja.sys.Sys;
import javax.net.ssl.HttpsURLConnection;

import com.accruent.telemetry.BRemoteSocketInfo;

public class AccruentCommunications
{

  final static int MAXLOOP = 60;
  public final static int SEND_CHANNEL_META_DATA = 1;
  public final static int SEND_TELEMETRY_DATA = 2;
  public final static int SEND_SETPOINT_DATA = 3;
  public final static char ETX = 3;
  public final static char EOT = 4;
  
  BRemoteSocketInfo bRemoteSocketInfo = null;
  Logger logger = null;

  String bsUrl = null;
  
  public AccruentCommunications(BRemoteSocketInfo bRemoteSocketInfop, Logger loggerp)
  {
    bRemoteSocketInfo = bRemoteSocketInfop;
    this.logger = loggerp;
  }
  
  public AccruentCommunications(String sUrlp, Logger loggerp)
  {
    this.bsUrl = sUrlp;
    this.logger = loggerp;
  }
  
  /*
   * Send a command and a payload in the format of 
   *    Station Name
   *    etx
   *    Local IP Address
   *    etx
   *    command            SEND_CHANNEL_META_DATA  or SEND_CHANNEL_DATA
   *    etx
   *    size of payload    sizeOf payload 
   *    etx
   *    payload            payload
   *    eot
   *    
   *  Returns    CommunicationResults
   */
  
  public CommunicationResults callHostHttp(int command, String infoToSend, String fileName) throws Exception
  {
    boolean isGoodTransmission = false;
    byte[] bIn = null;
    StringBuffer sbIn = null;
    CommunicationResults communicationResult = new CommunicationResults();
    LocalPlatform localPlatform = null;
    String ipAddresses = null;
    DataInputStream dis = null;
    DataOutputStream dos = null;
    URL url = new URL(bsUrl.toString());
    HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
    httpUrlConnection.setRequestMethod("POST");
    httpUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
    httpUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    httpUrlConnection.setRequestProperty("enctype", "multipart/form-data");
    httpUrlConnection.setRequestProperty("Content-Length", "0");
    httpUrlConnection.setDoInput(true);
    httpUrlConnection.setDoOutput(true);
    httpUrlConnection.setChunkedStreamingMode(1024);
    httpUrlConnection.connect();
    
    localPlatform = LocalPlatform.make();
    for(String id : localPlatform.getTcpIpManager().getAdapterIds())
    {
      if(null == ipAddresses)
      {
         if(null != localPlatform.getTcpIpManager().getIpAddress(id) && !localPlatform.getTcpIpManager().getIpAddress(id).equals("") )
         {  
            ipAddresses = localPlatform.getTcpIpManager().getIpAddress(id);
         }   
      }
      else
      {
         if(null != localPlatform.getTcpIpManager().getIpAddress(id)  && !localPlatform.getTcpIpManager().getIpAddress(id).equals(""))
         {  
            ipAddresses = String.format("%s|%s", ipAddresses, localPlatform.getTcpIpManager().getIpAddress(id));
         }   
      }
    }
    String outputBuffer = new String(String.format("stationName=%s&hostAddress=%s&command=%d&length=%d&info=%s",
                                                   Sys.getStation().getStationName(), 
                                                   ipAddresses, 
                                                   command, 
                                                   infoToSend.length(), 
                                                   infoToSend
                                                   ));

    dos = new DataOutputStream(httpUrlConnection.getOutputStream());
    if(null != fileName)
    {
      dos.write(String.format("%s&configzipped=",outputBuffer).getBytes());
      FilePath fp = new FilePath(fileName);
      BIFile biFile = BFileSystem.INSTANCE.makeFile(fp);
      DataInputStream dis1 = new DataInputStream(biFile.getInputStream());
      byte[] bIn1 = null;
      while(dis1.available() != 0)
      {
        bIn1 = new byte[dis1.available()];
        dis1.read(bIn1);
        for(int i : bIn1)
        {
          dos.write(String.valueOf(i).getBytes());
          dos.write(",".getBytes());
        }
      }
      dis1.close();
    }
    else
    {
      dos.write(outputBuffer.getBytes());
    }
    dos.flush();
    dos.close();
    dis = new DataInputStream(httpUrlConnection.getInputStream());
    logger.log(Level.INFO, "Loop Starting");
    for (int iX = 0; iX < MAXLOOP; iX++)
    {
      Thread.sleep(1000);
      while (dis.available() > 0)
      {
        bIn = new byte[dis.available()];
        dis.readFully(bIn);
        Thread.sleep(1000);
        if(null == sbIn)
        {
          sbIn = new StringBuffer(new String(bIn));
        }
        else
        {
          sbIn.append(new String(bIn));
        }
        if(sbIn.toString().indexOf(EOT) != -1 )
        {
          logger.log(Level.INFO, String.format("Looper = %d", iX));
          iX = MAXLOOP;
          isGoodTransmission = true;
        }
      }
    }
    dis.close();
    logger.log(Level.INFO, String.format("ReturnedInfo = %s", sbIn.toString()));
    logger.log(Level.INFO, "Loop Complete");
    communicationResult.setGoodTransmission(isGoodTransmission);
    communicationResult.setChannelsConfigured(null != sbIn ? sbIn.toString() : null);

    return communicationResult;
  }

  public CommunicationResults callHostHttps(int command, String infoToSend, String fileName) throws Exception
  {
    boolean isGoodTransmission = false;
    byte[] bIn = null;
    StringBuffer sbIn = null;
    LocalPlatform localPlatform = null;
    CommunicationResults communicationResult = new CommunicationResults();
    DataInputStream dis = null;
    DataOutputStream dos = null;
    URL url = new URL(bsUrl.toString());
    String ipAddresses = null;
    HttpsURLConnection httpUrlConnection = (HttpsURLConnection) url.openConnection();
    httpUrlConnection.setRequestMethod("POST");
    httpUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
    httpUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    httpUrlConnection.setRequestProperty("enctype", "multipart/form-data");
    httpUrlConnection.setRequestProperty("Content-Length", "0");
    httpUrlConnection.setDoInput(true);
    httpUrlConnection.setDoOutput(true);
    httpUrlConnection.setChunkedStreamingMode(1024);
    httpUrlConnection.connect();
    localPlatform = LocalPlatform.make();
    for(String id : localPlatform.getTcpIpManager().getAdapterIds())
    {
      if(null == ipAddresses)
      {
         ipAddresses = localPlatform.getTcpIpManager().getIpAddress(id);
      }
      else
      {
         ipAddresses = String.format("%s|%s", ipAddresses, localPlatform.getTcpIpManager().getIpAddress(id));         
      }
    }
    
    String outputBuffer = new String(String.format("stationName=%s&hostAddress=%s&command=%d&length=%d&info=%s",
                                                   Sys.getStation().getStationName(), 
                                                   ipAddresses, 
                                                   command, 
                                                   infoToSend.length(), 
                                                   infoToSend));
    dos = new DataOutputStream(httpUrlConnection.getOutputStream());
    if(null != fileName)
    {
      dos.write(String.format("%s&configzipped=",outputBuffer).getBytes());
      FilePath fp = new FilePath(fileName);
      BIFile biFile = BFileSystem.INSTANCE.makeFile(fp);
      DataInputStream dis1 = new DataInputStream(biFile.getInputStream());
      byte[] bIn1 = null;
      while(dis1.available() != 0)
      {
        bIn1 = new byte[dis1.available()];
        dis1.read(bIn1);
        for(int i : bIn1)
        {
          dos.write(String.valueOf(i).getBytes());
          dos.write(",".getBytes());
        }
      }
      dis1.close();
    }
    else
    {
      dos.write(outputBuffer.getBytes());
    }
    dos.flush();
    dos.close();

    dis = new DataInputStream(httpUrlConnection.getInputStream());
    logger.log(Level.INFO, "Loop Starting");
    for (int iX = 0; iX < MAXLOOP; iX++)
    {
      Thread.sleep(1000);
      while (dis.available() > 0)
      {
        bIn = new byte[dis.available()];
        dis.readFully(bIn);
        Thread.sleep(1000);
        if(null == sbIn)
        {
          sbIn = new StringBuffer(new String(bIn));
        }
        else
        {
          sbIn.append(new String(bIn));
        }
        if(sbIn.toString().indexOf(EOT) != -1 )
        {
          logger.log(Level.INFO, String.format("Looper = %d", iX));
          iX = MAXLOOP;
          isGoodTransmission = true;
        }
      }
    }
    dis.close();
    if(null != sbIn)
    {  
       logger.log(Level.INFO, String.format("ReturnedInfo = %s", sbIn.toString()));
    }
    else
    {
      logger.log(Level.INFO, String.format("Nothing Returned"));
    }
    logger.log(Level.INFO, "Loop Complete");
    communicationResult.setGoodTransmission(isGoodTransmission);
    communicationResult.setChannelsConfigured(null != sbIn ? sbIn.toString() : null);

    return communicationResult;
  }

  
  
  public CommunicationResults callHost(int command, String infoToSend ) throws Exception
  {
    boolean isGoodTransmission = false;
    CommunicationResults communicationResult = new CommunicationResults();
    DataInputStream dis = null;
    BufferedOutputStream bos = null;
    byte[] bIn = null;
    Socket socket = null;
    String outputBuffer = new String(String.format("%s%c%s%c%d%c%d%c%s%c",
                                                   Sys.getStation().getStationName(), 
                                                   ETX, 
                                                   Sys.getLocalHost().getHostAddress(), 
                                                   ETX, 
                                                   command, 
                                                   ETX, 
                                                   infoToSend.length(), 
                                                   ETX, 
                                                   infoToSend, 
                                                   EOT));
    StringBuffer sbIn = null;
    socket = new Socket(bRemoteSocketInfo.getRemoteIpAddress(), bRemoteSocketInfo.getRemotePortNumber());
    socket.setSoTimeout(MAXLOOP * 1000);
    bos = new BufferedOutputStream(socket.getOutputStream());
    bos.write(outputBuffer.getBytes());
    bos.flush();
    dis = new DataInputStream(socket.getInputStream());
    logger.log(Level.INFO, "Loop Starting");
    for (int iX = 0; iX < MAXLOOP; iX++)
    {
      Thread.sleep(1000);
      while (dis.available() > 0)
      {
        bIn = new byte[dis.available()];
        dis.readFully(bIn);
        Thread.sleep(1000);
        if(null == sbIn)
        {
          sbIn = new StringBuffer(new String(bIn));
        }
        else
        {
          sbIn.append(new String(bIn));
        }
        if(sbIn.toString().indexOf(EOT) != -1 )
        {
          logger.log(Level.INFO, String.format("Looper = %d", iX));
          iX = MAXLOOP;
          isGoodTransmission = true;
        }
      }
    }
    logger.log(Level.INFO, String.format("ReturnedInfo = %s", sbIn.toString()));
    logger.log(Level.INFO, "Loop Complete");
    dis.close();
    bos.close();
    socket.close();
    communicationResult.setGoodTransmission(isGoodTransmission);
    communicationResult.setChannelsConfigured(null != sbIn ? sbIn.toString() : null);
    return communicationResult;
  }
  
  public String getBsUrl()
  {
    return bsUrl;
  }

  public void setBsUrl(String bsUrl)
  {
    this.bsUrl = bsUrl;
  }

  public class CommunicationResults
  {
    boolean isGoodTransmission = true;
    String channelsConfigured = null;
    public CommunicationResults()
    {
      
    }
    public boolean isGoodTransmission()
    {
      return isGoodTransmission;
    }
    public void setGoodTransmission(boolean isGoodTransmission)
    {
      this.isGoodTransmission = isGoodTransmission;
    }
    public String getChannelsConfigured()
    {
      return channelsConfigured;
    }
    public void setChannelsConfigured(String channelsConfigured)
    {
      this.channelsConfigured = channelsConfigured;
    }
  }
}
